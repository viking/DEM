/*
 * viking -- GPS Data and Topo Analyzer, Explorer, and Manager
 *
 * Copyright (C) 2003-2005, Evan Battaglia <gtoevan@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* GdkPixbuf RGB C-Source image dump 1-byte-run-length-encoded */

static const GdkPixdata mapslayer_pixbuf = {
  0x47646b50, /* Pixbuf magic: 'GdkP' */
  24 + 318, /* header length + pixel_data length */
  0x2010001, /* pixdata_type */
  48, /* rowstride */
  16, /* width */
  16, /* height */
  /* pixel_data: */
  "\1\6q6\216\10\202\26\1\5^[\216\10\202\26\3\7}\40\1=\234\7t2\204\10\202"
  "\26\1\10\202\27\207\10\202\26\5\7}\40\2E\214\5eN\0""3\257\6r6\203\10"
  "\202\26\15\7|!\3M|\1""9\244\2H\206\5fM\10\200\31\10\201\27\5`Y\1=\234"
  "\5aU\10\202\26\0""0\266\5cR\203\10\202\26\1\4Ye\204\0/\267\4\1>\232\3"
  "M}\3Pw\7}\40\202\10\202\26\1\10\200\31\204\10\202\26\3\10\200\32\3M}"
  "\0""2\261\203\0/\267\1\3M}\202\10\202\26\1\10\202\27\210\10\202\26\1"
  "\3Qu\203\0/\267\1\2@\226\213\10\202\26\1\5aV\203\0/\267\1\1\77\231\214"
  "\10\202\26\1\3Sq\202\0/\267\1\4Zd\207\10\202\26\1\16\177\26\204\10\202"
  "\26\4\10~\36\2A\224\2D\216\7{#\206\10\202\26\3$t\26\330\31\30T\\\27\204"
  "\10\202\26\1\10\201\30\210\10\202\26\3\14\200\26lP\27+p\26\232\10\202"
  "\26\1\10\202\27\217\10\202\26\1\10}\37\214\10\202\26\4\10\201\27\6l@"
  "\3M}\0""1\263\213\10\202\26\2\5fL\1""9\244\203\0/\267",
};
