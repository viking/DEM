/*
 * viking -- GPS Data and Topo Analyzer, Explorer, and Manager
 *
 * Copyright (C) 2003-2005, Evan Battaglia <gtoevan@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* GdkPixbuf RGB C-Source image dump 1-byte-run-length-encoded */

static const GdkPixdata aggregatelayer_pixbuf = {
  0x47646b50, /* Pixbuf magic: 'GdkP' */
  24 + 576, /* header length + pixel_data length */
  0x2010001, /* pixdata_type */
  48, /* rowstride */
  16, /* width */
  16, /* height */
  /* pixel_data: */
  "\206\377\377\377\2\330\332\325\250\254\245\216\377\377\377\3d^Q\214\257"
  "z\260\261\256\213\377\377\377\10\337\337\337\224\212\206[g@\200\256g"
  "dgJQA\37df5\214\222\203\207\377\377\377\12\202\201q\\n>srBs\220Qr\247"
  "X\230\266\200pv:{\246V\210\253]\211\226y\205\377\377\377\14\244\244\241"
  "h\224L\206\265e}\240p\\W1{\255X\250\317\220\207\252_qu:yv5~\257Zv\205"
  "m\204\377\377\377\15aiVz\260]u\253W~\242Yr\226O\177\257`\200\231^y\234"
  "Pn\206D|\242I\217\301n}\247p\317\317\317\203\377\377\377\15\200\211f"
  "q\237To\236Xj\244]n\261]\205\276}\207\253\177c\231I\202\272k\251\317"
  "\254\234\306\215\230\304\207\247\250\245\203\377\377\377\15\211\213g"
  "s\231Zr\231`o\237_l\250Y\211\276\206\214\270\216V\2057k\246R\214\274"
  "|\216\271n\214\272h\216\220\212\203\377\377\377\15\205v_lq>q{Gg\205H"
  "h\226Kv\247Xo\234Q[\2056d\225\77y\246R\202\256Zm\225N\301\301\300\203"
  "\377\377\377\15a\\Wkg8xj9qu>l\177@w\237Up\206Cd\2046k\221\77t\235Ix\247"
  "P^\202D\277\277\277\203\377\377\377\15\277\277\277V>%wl:to:rz>u\212F"
  "wx>ks0l\2058r\231D|\252Sg\216M\277\277\277\204\377\377\377\14\77&\31"
  "ue9s\\3sv\77qy;sp9gt3p\224Gu\240L}\256Uf\211F\277\277\277\204\377\377"
  "\377\13eQLrT2rd6q~Co|>q_2kX*m\177:r\236F\177\253OXf<\205\377\377\377"
  "\13\213\203\202h9%v\\2tz@uz=qI+j4\36mf/o\234BZ\177;\243\243\241\205\377"
  "\377\377\12\357\357\357G\"\34yE)l=&vK,J\32\17G\22\14B\34\17FK2\267\271"
  "\265\207\377\377\377\7\320\317\317\217\202\201xie\216\206\204\300\277"
  "\277\277\277\277\337\337\337\205\377\377\377",
};


